#include "RandomGenerator.h"

#include <stdlib.h>	/* srand, rand */
#include <time.h>	/* time */
#include <math.h> 	/* fmod */

using namespace si;

std::shared_ptr< RandomGenerator > RandomGenerator::inst = NULL;

std::shared_ptr< RandomGenerator >& RandomGenerator::instance() {
	// Do "lazy initialization" in the accessor function
	if (!inst) {
		inst.reset(new RandomGenerator());
	}
	return inst;
}

void RandomGenerator::setChance(double c) {
	chance = c * 100;
}

int RandomGenerator::getNumber() {
	// Generate a number between 0.0 and 100.0
	double random = (rand() % 1001) / 10.0;

	if (random < chance) return 1;
	return 0;
}

RandomGenerator::RandomGenerator() {
	// Initialize random seed
	srand(time(NULL));
}