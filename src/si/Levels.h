#ifndef LEVELS_H_
#define LEVELS_H_

#include <vector>
#include <string>
#include <map>
#include <utility>

namespace si {

	// Helper functions
	void errorMessage();
	void validAnswer(std::string&, int);

	class Levels {
	public:
		Levels();

		void loadXML();
		std::pair<std::string, unsigned int> determineToPlay();
		std::string getLevel(std::pair<std::string, unsigned int>);
		bool exists(std::pair<std::string, unsigned int>);
	private:
		std::map< std::string, std::vector< std::string > > levels;
	};

} /* End of namespace si */

#endif /* LEVELS_H_ */