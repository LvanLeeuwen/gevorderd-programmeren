#ifndef RANDOMGENERATOR_H_
#define RANDOMGENERATOR_H_

#include <memory>

namespace si {

	class RandomGenerator {
		/* 
		 * This class is implemented according to the singleton pattern
		 */
	public:
		// Define a public static accessor function
		static std::shared_ptr< RandomGenerator >& instance();

		void setChance(double c);
		int getNumber();
	private:
		// Define a private static attribute
		static std::shared_ptr< RandomGenerator > inst;
		
		double chance;

		RandomGenerator();
	};

} /* End of namespace si */

#endif /* RANDOMGENERATOR_H_ */
