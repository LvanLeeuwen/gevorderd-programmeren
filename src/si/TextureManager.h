#ifndef TEXTUREMANAGER_H_
#define TEXTUREMANAGER_H_

#include <SFML/Graphics.hpp>
#include <map>
#include <string>

/* Inspired on https://www.binpress.com/tutorial/creating-a-city-building-game-with-sfml-part-3-textures-and-animations/125
 * Consulted on 4-12-2015
 */

namespace si {

	class TextureManager {
	public:
		TextureManager();

		void loadTexture(const std::string name, const std::string filename);

		sf::Texture& getTexture(const std::string name);
		sf::Vector2u getTextureSize(const std::string name);
	private:
		static std::map< std::string, sf::Texture> textures;
	};

} /* End of namespace si */

#endif /* TEXTUREMANAGER_H_ */
