#include "TextureManager.h"
#include <iostream>
#include <exception>

using namespace si;

std::map< std::string, sf::Texture > TextureManager::textures;

TextureManager::TextureManager() {}

void TextureManager::loadTexture(const std::string name, const std::string filename) {
	// Don't load the texture if it's already loaded.
	if (textures.find(name) != textures.end()) return;

	// Load the texture if possible
	sf::Texture texture;
	if (!texture.loadFromFile("../../src/Input/Pictures/" + filename)) {
		throw std::runtime_error("Unable to load picture: " + filename);
	}
	
	textures[name] = texture;
}

sf::Texture& TextureManager::getTexture(const std::string name) {
	// Return the texture if it's present
	std::map< std::string, sf::Texture >::iterator search = textures.find(name);
	if (search == textures.end()) {
		throw std::runtime_error("No entry with key " + name + " in map");
	}

	return textures[name];
}

sf::Vector2u TextureManager::getTextureSize(const std::string name) {
	// Return the size of the texture if it's present
	std::map< std::string, sf::Texture >::iterator search = textures.find(name);
	if (search == textures.end()) {
		throw std::runtime_error("No entry with key " + name + " in map");
	}
	
	sf::Vector2u size = textures[name].getSize();
	return size;
}