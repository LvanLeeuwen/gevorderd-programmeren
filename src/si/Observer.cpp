#include "Observer.h"

using namespace si;

//--------------------------------------------------------------------------------------------------------------
//// REPRESENTATION
//--------------------------------------------------------------------------------------------------------------

Representation::Representation(const std::string _kind, const double _scale, const unsigned int _x, const unsigned int _y) { 
	// We still need to calculate the position if it's called for a youwin or gameover representation.
	if (_kind == "youwin" || _kind == "gameover") {
		x = _x / 2 - si::view::View::textures.getTextureSize(_kind).x / 2;
		y = _y / 2 - si::view::View::textures.getTextureSize(_kind).y / 2;
	}
	else {
		x = _x;
		y = _y;
	}
	scale = _scale;
	si::view::View::buffer[this] = _kind;
}

void Representation::update(unsigned int _x, unsigned int _y) {
	x = _x;
	y = _y;
}

void Representation::update(std::string s) {
	si::view::View::buffer[this] = s;
}

void Representation::removeFromBuffer() {
	si::view::View::buffer.erase(this);
}

unsigned int Representation::getX() const {
	return x;
}

unsigned int Representation::getY() const {
	return y;
}

double Representation::getScale()  const {
	return scale;
}