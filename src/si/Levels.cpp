#include "Levels.h"

#include <dirent.h> /* DIR, dirent */
#include <unistd.h> /* get_current_dir_name */
#include <algorithm>
#include <iostream>
#include <exception>

using namespace si;

void si::errorMessage() {
	std::cout << "Please choose a valid answer, try again: ";
}

void si::validAnswer(std::string& s, int size) {
	/*
	 * Check if the answer is valid
	 * If not, keep asking until the answer is valid
	 */
	while (true) {
		std::cin >> s;
		if (!all_of(s.begin(), s.end(), ::isdigit)) {
			errorMessage();
			continue;
		}
		int temp = atoi(s.c_str());
		if (temp > 0 && temp <= size) return;
		else errorMessage();
	}
}

Levels::Levels() {}

void Levels::loadXML() {
	/*
	 * This function loads the xmlfiles in the map.
	 */
	//std::vector< std::string > result;
	std::string dir, filepath, str;
	DIR * dp;
	struct dirent *dirp;
	const char *buf;

	buf = (char *)get_current_dir_name();
	std::string temp(buf);
	temp = temp.substr(0, temp.size()-10);
	temp += "/src/Input/Levels";
	buf = temp.c_str();

	dp = opendir(buf);
	if (dp != NULL) {
		while ( dirp = readdir(dp) ) {
			filepath = "../../src/Input/Levels/" + dir + dirp->d_name;
			//std::cout << filepath << std::endl;
			if (filepath.size() >= 4) {
				if (filepath.substr(filepath.size() - 4) == ".xml") {
					std::string key = filepath.substr(0, filepath.size() - 12);
	 				levels[key].push_back(filepath.substr(key.size() + 1));
				}
			}
		}
	}
	else throw std::runtime_error("Directory not found");

	closedir(dp);
/*
	boost::filesystem::path p("../../src/Input/Levels");
	boost::filesystem::directory_iterator end_itr;

	for (boost::filesystem::directory_iterator itr(p); itr != end_itr; ++itr) {
	 	if (is_regular_file(itr->path())) {
	 		std::string current_file = itr->path().string();
	 		if (current_file.substr(current_file.size() - 4) == ".xml") {
	 			std::string key = current_file.substr(0, current_file.size() - 12);
	 			levels[key].push_back(current_file.substr(key.size() + 1));
	 		}
	 	}
	}
	*/
}

std::pair<std::string, unsigned int> Levels::determineToPlay() {
	/*
	 * This function determines which function the player wants to play.
	 */

	unsigned int index = 1;
	std::string version, level;
	std::map< unsigned int, std::string> v;

	if (levels.size() == 0) throw std::runtime_error("No levels available");
	
	// Get the version
	for (auto i : levels) {
		std::string str;
		std::size_t found = i.first.find("Levels/");
		if (found != std::string::npos) {
			str = i.first.substr(found + 7);
		}
		else throw std::runtime_error("Incorrect level");

		std::cout << index << ") " << str << std::endl;
		v[index] = i.first;
		index++;
	}

	std::cout << "Which version of Space Invaders do you want to play?\n>>>> ";
	validAnswer(version, levels.size());
	version = v[atoi(version.c_str())];

	// Get the level
	if (levels[version].size() == 0) throw std::runtime_error("No levels available");
	std::sort(levels[version].begin(), levels[version].end());
	for (unsigned int i = 1; i <= levels[version].size(); i++) {
		std::cout << i << ") " << levels[version][i-1] << std::endl;
	}

	std::cout << "Which level do you want to play?\n>>>> ";
	validAnswer(level, levels[version].size());

	return std::make_pair(version, atoi(level.c_str()) - 1);
}

std::string Levels::getLevel(std::pair<std::string, unsigned int> p) {
	return p.first + " " + levels[p.first][p.second];
}

bool Levels::exists(std::pair<std::string, unsigned int> p) {
	if (p.second + 1 > levels[p.first].size()) return false;
	return true;
}