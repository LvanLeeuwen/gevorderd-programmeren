#include "Stopwatch.h"
#include <iostream>

using namespace si;

Stopwatch::Stopwatch() {
	start = std::chrono::steady_clock::now();
}

double Stopwatch::getDifference() {
	std::chrono::time_point< std::chrono::steady_clock > end = std::chrono::steady_clock::now();
	std::chrono::duration< double > result = end - start;
	
	return result.count();
}

void Stopwatch::reset() {
	std::chrono::time_point< std::chrono::steady_clock > end = std::chrono::steady_clock::now();
	start = end;
}