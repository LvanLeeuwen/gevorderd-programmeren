#include "Parser.h"
#include "TextureManager.h"

#include <exception>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/optional/optional.hpp>

using namespace si;

Parser::Parser(std::string filename) {
	world = std::make_shared< si::model::World >();
	// Put the XML in a property tree
	read_xml(filename, xmlTree);
}

std::shared_ptr< si::model::World > Parser::getWorld() {
	return world;
}

void Parser::read() {
	// Create the textureManager and load it with the standard pictures
	TextureManager TexManager;
	std::string filename = xmlTree.get<std::string>("WORLD.PICTURESET") + "/";
	TexManager.loadTexture("bullet", filename + "bullet.png");
	TexManager.loadTexture("youwin", filename + "youwin.png");
	TexManager.loadTexture("gameover", filename + "gameover.png");

	// Set the properties of the world
	world->setProperties(xmlTree.get<std::string>("WORLD.NAME"), 
							xmlTree.get<std::string>("WORLD.PICTURESET"), 
							xmlTree.get<double>("WORLD.SPEED"), 
							xmlTree.get<double>("WORLD.BULLETSPEED"), 
							xmlTree.get<int>("WORLD.MOVE")
							);
	
	// Set the dimensions of the world
	world->setX(xmlTree.get<unsigned int>("WORLD.SIZE.<xmlattr>.width"));
	world->setY(xmlTree.get<unsigned int>("WORLD.SIZE.<xmlattr>.height"));

	// Set the lives
	double scale = xmlTree.get<double>("WORLD.LIVES.<xmlattr>.scale");
	BOOST_FOREACH(boost::property_tree::ptree::value_type const& v, xmlTree.get_child("WORLD.LIVES")) {
		if (v.first == "LIVE") {
			world->setLive(
				  v.second.get<unsigned int>("<xmlattr>.x")
				, v.second.get<unsigned int>("<xmlattr>.y")
				, scale
				);
		}
	}
	TexManager.loadTexture("live", filename + "live.png");

	// Set the shields if possible
	boost::optional< boost::property_tree::ptree& > child = xmlTree.get_child_optional("WORLD.SHIELDS");
	if (child) {
		// Load the textures for the shields
		for (unsigned int s=1; s<=3; s++)
			TexManager.loadTexture("shield" + std::to_string(s), filename + "shield" + std::to_string(s) + ".png");

		scale = xmlTree.get<double>("WORLD.SHIELDS.<xmlattr>.scale");
		BOOST_FOREACH(boost::property_tree::ptree::value_type const& v, xmlTree.get_child("WORLD.SHIELDS")) {
			if (v.first == "SHIELD") {
				world->setShield(
					  v.second.get<unsigned int>("<xmlattr>.x")
					, v.second.get<unsigned int>("<xmlattr>.y")
					, scale
					);
			}
		}
	}
	
	// Set the player
	world->setPlayer(
		  xmlTree.get<unsigned int>("WORLD.PLAYER.<xmlattr>.x")
		, xmlTree.get<unsigned int>("WORLD.PLAYER.<xmlattr>.y")
		, xmlTree.get<double>("WORLD.PLAYER.<xmlattr>.scale")
		);
	TexManager.loadTexture("player", filename + "player.png");

	// Set the enemies
	TexManager.loadTexture("enemy", filename + "enemy.png");
	world->getEnemies()->bulletRate = xmlTree.get<double>("WORLD.ENEMIES.<xmlattr>.bulletRate");
	world->getEnemies()->randomAppearRate = xmlTree.get("WORLD.ENEMIES.<xmlattr>.randomAppearRate", 0.0);

	if (xmlTree.get<std::string>("WORLD.ENEMIES.<xmlattr>.direction") == "Right")
		world->getEnemies()->direction = si::model::Direction::Right;
	else if (xmlTree.get<std::string>("WORLD.ENEMIES.<xmlattr>.direction") == "Left")
		world->getEnemies()->direction = si::model::Direction::Left;
	else if (xmlTree.get<std::string>("WORLD.ENEMIES.<xmlattr>.direction") == "Up")
		world->getEnemies()->direction = si::model::Direction::Up;
	else if (xmlTree.get<std::string>("WORLD.ENEMIES.<xmlattr>.direction") == "Down")
		world->getEnemies()->direction = si::model::Direction::Down;
	else throw std::runtime_error("No direction for enemy found");
	
	BOOST_FOREACH(boost::property_tree::ptree::value_type const& v, xmlTree.get_child("WORLD.ENEMIES")) {
		if (v.first == "ENEMY") {
			world->setEnemy(
				  v.second.get<unsigned int>("<xmlattr>.x")
				, v.second.get<unsigned int>("<xmlattr>.y")
				, v.second.get<double>("<xmlattr>.scale")
				);
		}
	}
}
