#ifndef OBSERVER_H_
#define OBSERVER_H_

#include "../si_view/View.h"

#include <string>

namespace si {

	class Observer {
	public:
		virtual void update(unsigned int _x, unsigned int _y) = 0;
		virtual void update(std::string s) = 0;
		virtual void removeFromBuffer() = 0;
	};


	class Representation : public Observer {
	public:
		Representation() : Observer() {}
		Representation(const std::string _kind, const double _scale, const unsigned int _x = 0, const unsigned int _y = 0);
	
		void update(unsigned int _x, unsigned int _y);
		void update(std::string s);
		void removeFromBuffer();

		unsigned int getX() const;
		unsigned int getY() const;
		double getScale() const;
	private:
		unsigned int x;
		unsigned int y;
		double scale;
	};

} /* End of namespace si */

#endif /* OBSERVER_H_ */