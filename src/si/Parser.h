#ifndef PARSER_H_
#define PARSER_H_

#include "../si_model/GameObjects.h"

#include <boost/property_tree/ptree.hpp>

#include <memory>
#include <string>

namespace si {

	class Parser {
	public:
		Parser(std::string filename);

		std::shared_ptr< si::model::World > getWorld();
		
		void read();
	private:
		boost::property_tree::ptree xmlTree;
		std::shared_ptr< si::model::World > world;
	};

} /* End of namespace si */

#endif /* PARSER_H_ */
