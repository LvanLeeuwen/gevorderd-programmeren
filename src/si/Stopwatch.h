#ifndef STOPWATCH_H_
#define STOPWATCH_H_

#include <chrono>

namespace si {

	class Stopwatch {
	public:
		Stopwatch();

		double getDifference();
		void reset();
	private:
		std::chrono::time_point< std::chrono::steady_clock > start;
	};

} /* End of namespace si */

#endif /* STOPWATCH_H_ */