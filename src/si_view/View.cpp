#include "View.h"
#include <iostream>

using namespace si::view;

std::shared_ptr< sf::RenderWindow > View::window(new sf::RenderWindow());
std::map< si::Representation*, std::string > View::buffer;
si::TextureManager View::textures;

View::View() {}

void View::setWindow(unsigned int width, unsigned int height, std::string name) {
	window->create(sf::VideoMode(width, height), "Space Invaders - " + name);
	clearWindow();
}

void View::clearWindow() {
	window->clear(background_color);
}

void View::draw() {
	// Loop through the buffer
	std::map< si::Representation*, std::string >::iterator it;
	for (it = buffer.begin(); it != buffer.end(); it++) {
		// Create a shape
		sf::Sprite shape(textures.getTexture(it->second));
		shape.scale(it->first->getScale(), it->first->getScale());
		shape.setPosition(it->first->getX(), it->first->getY());
		window->draw(shape);
	}
}

std::map< si::Representation*, std::string >& View::getBuffer() {
	return buffer;
}