#ifndef VIEW_H_
#define VIEW_H_

#include <SFML/Graphics.hpp>
#include <map>
#include <memory>

#include "../si/Observer.h"
#include "../si/TextureManager.h"

namespace si {

	class Representation;

	namespace view {

		class View {
		public:
			friend class si::Representation;

			View();
			
			void setWindow(unsigned int width, unsigned int height, std::string name);
			void clearWindow();
			void draw();

			std::map< si::Representation*, std::string >& getBuffer();

			static std::shared_ptr< sf::RenderWindow > window;
		private:
			const sf::Color background_color = sf::Color::Black;
			static std::map< si::Representation*, std::string > buffer;
			static TextureManager textures;
		};

	} /* End of namespace view */
} /* End of namespace si */

#endif /* VIEW_H_ */