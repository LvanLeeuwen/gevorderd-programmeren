#ifndef ENTITY_H_
#define ENTITY_H_

#include <memory>
#include <vector>
#include <string>
#include "../si/Observer.h"
#include <iostream>
class Observer;

namespace si {

	namespace model {

		class Entity {
		public:
			Entity();
			Entity(unsigned int _x, unsigned int _y, double _scale);

			int getX() const;
			int getY() const;
			double getScale() const;
			
			void setX(unsigned int _x);
			void setY(unsigned int _y);

			std::vector< std::shared_ptr< si::Observer > > getObservers();
			void attach(std::shared_ptr< si::Observer > obs);
			void notify();
		private:
			int x;
			int y;
			double scale;
			std::vector< std::shared_ptr< si::Observer > > observers;
		};


		class MovableEntity : public Entity {
		public:
			MovableEntity() : Entity() {}
			MovableEntity(unsigned int _x, unsigned int _y, double _scale) : Entity(_x, _y, _scale) {}

			virtual void move(int _x, int _y) = 0;			
		};

	} /* End of namespace model */
} /* End of namespace si */

#endif /* ENTITY_H_ */
