#ifndef GAMEOBJECTS_H_
#define GAMEOBJECTS_H_

#include "Entity.h"
#include <map>

namespace si {

	namespace model {

		enum Direction {Up, Down, Right, Left};

		enum GameStatus {Unknown, Won, Lost, Paused};


		class Ship : public MovableEntity {
		public:
			Ship() : MovableEntity() {}
			Ship(unsigned int _x, unsigned int _y, double _scale) : MovableEntity(_x, _y, _scale) {} 
		
			void move(int _x, int _y);
		};


		class Bullet : public MovableEntity {
		public:
			Bullet() : MovableEntity() {}
			Bullet(const unsigned int _x, const unsigned int _y, const double _scale, const Direction _direction);

			void move(int _x, int _y);

			Direction getDirection() const;
		private:
			Direction direction;
		};


		struct Enemy {
			Direction direction;
			double bulletRate;
			double randomAppearRate;
			std::vector< std::shared_ptr< Ship > > myEnemies;
		};

		struct WorldProperties {
			std::string name;
			std::string pictureSet;
			double speed;
			double bullet_speed;
			int move_amount;
		};


		class World : public Entity {
		public:
			World();

			WorldProperties getProperties() const;
			GameStatus getStatus() const;
			std::vector< std::shared_ptr< Entity > >& getLives();
			std::map< std::shared_ptr< Entity >, std::string >& getShields();
			std::shared_ptr< Enemy > getEnemies() const;
			std::shared_ptr< Ship >& getPlayer();
			std::vector< std::shared_ptr< Bullet > >& getBullets();

			void setProperties(std::string n, std::string p, double s, double b, int m);
			void setStatus(GameStatus s);
			void setLive(unsigned int _x, unsigned int _y, double _scale);
			void setShield(unsigned int _x, unsigned int _y, double _scale);
			void setPlayer(unsigned int _x, unsigned int _y, double _scale);
			void setPlayer(std::shared_ptr< Ship > p);
			void setEnemy(unsigned int _x, unsigned int _y, double _scale);
			void setBullet(const unsigned int _x, const unsigned int _y, const Direction _direction);
			
			void setEndGame();

			void clearEnemies();
		private:
			WorldProperties properties;
			GameStatus status;
			std::vector< std::shared_ptr< Entity > > lives;
			std::map< std::shared_ptr< Entity >, std::string > shields;
			std::shared_ptr< Ship > player;
			std::shared_ptr< Enemy > enemies;
			std::vector< std::shared_ptr< Bullet > > bullets;
		};

	} /* End of namespace model */

} /* End of namespace si */

#endif /* GAMEOBJECTS_H_ */