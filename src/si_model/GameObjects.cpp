#include "GameObjects.h"
#include "../si/Observer.h"

using namespace si::model;

//--------------------------------------------------------------------------------------------------------------
//// SHIP
//--------------------------------------------------------------------------------------------------------------

void Ship::move(int _x, int _y) {
	this->setX(this->getX() + _x);
	this->setY(this->getY() + _y);
	
	notify();
}

//--------------------------------------------------------------------------------------------------------------
//// BULLET
//--------------------------------------------------------------------------------------------------------------

Bullet::Bullet(const unsigned int _x, const unsigned int _y, const double _scale, const Direction _direction) 
	: MovableEntity(_x, _y, _scale) {

	direction = _direction;

	// Set observer
	std::shared_ptr< si::Representation > obsBullet(new si::Representation("bullet", _scale, _x, _y));
	attach(obsBullet);
	notify();
}

void Bullet::move (int _x, int _y) {
	this->setX(this->getX() + _x);
	this->setY(this->getY() + _y);
	
	notify();
}

Direction Bullet::getDirection() const {
	return direction;
}

//--------------------------------------------------------------------------------------------------------------
//// WORLD
//--------------------------------------------------------------------------------------------------------------

World::World() : Entity() {
	enemies = std::make_shared< Enemy >();
	status = Unknown;
}

WorldProperties World::getProperties() const {
	return properties;
}

GameStatus World::getStatus() const {
	return status;
}

std::vector< std::shared_ptr< Entity > >& World::getLives() {
	return lives;
}

std::map< std::shared_ptr< Entity >, std::string >& World::getShields() {
	return shields;
}

std::shared_ptr< Enemy > World::getEnemies() const {
	return enemies;
}

std::vector< std::shared_ptr< Bullet > >& World::getBullets() {
	return bullets;
}

std::shared_ptr< Ship >& World::getPlayer() {
	return player;
}

void World::setProperties(std::string n, std::string p, double s, double b, int m) {
	properties.name = n;
	properties.pictureSet = p;
	properties.speed = s;
	properties.bullet_speed = b;
	properties.move_amount = m;
}

void World::setStatus(GameStatus s) {
	status = s;
}

void World::setLive(unsigned int _x, unsigned int _y, double _scale) {
	lives.push_back(std::make_shared< Entity >(_x, _y, _scale));

	// Set the observer
	std::shared_ptr< si::Representation > obsLive(new si::Representation("live", _scale, _x, _y));
	lives[lives.size()-1]->attach(obsLive);
}

void World::setShield(unsigned int _x, unsigned int _y, double _scale) {
	std::shared_ptr< Entity > newEntity(new Entity(_x, _y, _scale));

	// Set the observer
	std::shared_ptr< si::Representation > obsShield(new si::Representation("shield1", _scale, _x, _y));
	newEntity->attach(obsShield);

	shields[newEntity] = "shield1";
}

void World::setPlayer(unsigned int _x, unsigned int _y, double _scale) {
	player = std::make_shared< Ship >(_x, _y, _scale);

	// Set the observer
	std::shared_ptr< si::Representation > obsPlayer(new si::Representation("player", _scale, _x, _y));
	player->attach(obsPlayer);
}

void World::setPlayer(std::shared_ptr< Ship > p) {
	player = p;
}

void World::setEnemy(unsigned int _x, unsigned int _y, double _scale) {
	enemies->myEnemies.push_back(std::make_shared< Ship >(_x, _y, _scale));
	
	// Set the observer
	std::shared_ptr< si::Representation > obsEnemy(new si::Representation("enemy", _scale, _x, _y));
	enemies->myEnemies[enemies->myEnemies.size() - 1]->attach(obsEnemy);
}

void World::setBullet(const unsigned int _x, const unsigned int _y, const Direction _direction) {
	bullets.push_back(std::make_shared< Bullet >(_x, _y, player->getScale(), _direction));
}

void World::setEndGame() {
	if (status == Won)
		std::unique_ptr< si::Representation > obsGameWon(new si::Representation("youwin", 1, this->getX(), this->getY()));
	else
		std::unique_ptr< si::Representation > obsGameWon(new si::Representation("gameover", 1, this->getX(), this->getY()));
}

void World::clearEnemies() {
	enemies->myEnemies.clear();
}