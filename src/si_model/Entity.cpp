#include "Entity.h"
#include "../si_view/View.h"

#include <typeinfo>
#include <iostream>

using namespace si::model;

//--------------------------------------------------------------------------------------------------------------
//// ENTITY
//--------------------------------------------------------------------------------------------------------------

Entity::Entity() {
	x = 0;
	y = 0;
	scale = 1;
}

Entity::Entity(unsigned int _x, unsigned int _y, double _scale) : x(_x), y(_y), scale(_scale) {}

int Entity::getX() const {
	return x;
}

int Entity::getY() const {
	return y;
}

double Entity::getScale() const {
	return scale;
}

void Entity::setX(unsigned int _x) {
	x = _x;
}

void Entity::setY(unsigned int _y) {
	y = _y;
}

std::vector< std::shared_ptr< si::Observer > > Entity::getObservers() {
	return observers;
}

void Entity::attach(std::shared_ptr< Observer > obs) {
	observers.push_back(obs);
}

void Entity::notify() {
	for (unsigned int i = 0; i < observers.size(); i++) {
		observers[i]->update(x, y);
	}
}

//--------------------------------------------------------------------------------------------------------------
//// MOVABLE ENTITY
//--------------------------------------------------------------------------------------------------------------
