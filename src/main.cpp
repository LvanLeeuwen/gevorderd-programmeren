#include <iostream>
#include <exception>

#include "si_model/GameObjects.h"
#include "si_view/View.h"
#include "si/Levels.h"
#include "si/Parser.h"
#include "si_controller/Controller.h"
#include "si/Stopwatch.h"
#include "si/RandomGenerator.h"

void play(si::Levels& level, std::pair<std::string, unsigned int>& toPlay) {
	// Get the file we want to parse
	std::string toParse = level.getLevel(toPlay);
	
	// Parse the correct XML
	si::Parser parser(toParse);
	parser.read();

	// Get the world
	std::shared_ptr< si::model::World > world = parser.getWorld();

	// Create the window
	si::view::View window;
	window.setWindow(world->getX(), world->getY(), world->getProperties().name);
	
	si::controller::Controller controller(world);
	si::Stopwatch stopwatch_main;
	si::Stopwatch stopwatch_bullets;

	while (window.window->isOpen()) {
		sf::Event event;
		window.clearWindow();

		if (world->getPlayer() == nullptr) {
			// The game has ended
			controller.endGame(window);

			/* Check if the player has won
			 * If so: play the next level if possible
			 * If not: do nothing
			 */
			if (world->getStatus() == si::model::GameStatus::Won) {
				toPlay.second++;
				if (level.exists(toPlay)) {
					window.window->close();
					play(level, toPlay);
					return;
				}
			}
		} 
		else if (world->getStatus() != si::model::GameStatus::Paused)
			controller.checkTicks(stopwatch_main, stopwatch_bullets);
		
		while (window.window->pollEvent(event)) {
			controller.checkEvents(window, event);
		}
		window.draw();

		window.window->display();
	}
}

int main()
{
	std::cout << "Running Space Invaders\n\n";

	try {
		// Determine the first level to play
		si::Levels level;
		level.loadXML();
		std::pair<std::string, unsigned int> toPlay = level.determineToPlay();

		// Play this level
		play(level, toPlay);
		
	} catch (std::exception& e) {
		std::cout << "An error has occured: " << e.what() << std::endl;
	}

	return 0;
}
