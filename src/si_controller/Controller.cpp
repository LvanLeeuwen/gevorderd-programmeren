#include "Controller.h"
#include "../si/RandomGenerator.h"

#include <iostream>
#include <cmath>
#include <stdlib.h>	/* srand, rand */
#include <time.h>	/* time */

using namespace si::controller;

Controller::Controller(std::shared_ptr< si::model::World > _world) : world(_world) {
	srand(time(NULL));
}

void Controller::checkEvents(si::view::View& window, sf::Event &event) 
{
	if (event.type == sf::Event::Closed) {
		window.window->close();
	}
	else if (event.type == sf::Event::LostFocus) {
		world->setStatus(si::model::GameStatus::Paused);
	}
	else if (event.type == sf::Event::GainedFocus) {
		world->setStatus(si::model::GameStatus::Unknown);
	}
	else if (event.type == sf::Event::KeyPressed) {
		if (event.key.code == sf::Keyboard::Escape) {
			window.window->close();
		}

		// Make sure you can't use the keys anymore when it's game over
		if (world->getPlayer() != nullptr) {
			if (event.key.code == sf::Keyboard::Left) {
				// Move the player to the left
				if (world->getPlayer()->getX() - world->getProperties().move_amount > 0) {
					world->getPlayer()->move(-1 * world->getProperties().move_amount, 0);
				}
			}
			else if (event.key.code == sf::Keyboard::Right) {
				// Move the player to the right
				if (world->getPlayer()->getX() + world->getProperties().move_amount < world->getX() - texManager.getTextureSize("player").x * world->getPlayer()->getScale()) {
					world->getPlayer()->move(world->getProperties().move_amount, 0);
				}
			}
			else if (event.key.code == sf::Keyboard::Space) {
				// Get the coordinates for the bullet
				int x = world->getPlayer()->getX() 
					+ (texManager.getTextureSize("player").x * world->getPlayer()->getScale()) / 2 
					- (texManager.getTextureSize("bullet").x * world->getPlayer()->getScale()) / 2;

				int y = world->getPlayer()->getY() - (texManager.getTextureSize("bullet").y * world->getPlayer()->getScale());

				world->setBullet(x, y, si::model::Direction::Up);
			}
		}
	}
}

void Controller::checkTicks(si::Stopwatch& stopwatch_main, si::Stopwatch& stopwatch_bullets) {
	/*
	 * This function moves the enemies and bullets based on the value of the stopwatch
	 */
	double diff = stopwatch_main.getDifference();
	if (diff >= world->getProperties().speed) {
		stopwatch_main.reset();

		this->checkRandomEnemies();
		this->moveEnemies();
		this->checkFireBullets();
	}

	diff = stopwatch_bullets.getDifference();
	if (diff >= world->getProperties().bullet_speed) {
		stopwatch_bullets.reset();

		this->moveBullets();
	}
	
	this->checkCollision();
}

void Controller::endGame(si::view::View& window) {
	// Empty the vectors for bullets
	world->getBullets().clear();
	
	// Empty the buffer from window
	window.getBuffer().clear();

	if (world->getStatus() == si::model::GameStatus::Unknown && world->getEnemies()->myEnemies.size() == 0) {
		world->setStatus(si::model::GameStatus::Won);
	}
	else if (world->getStatus() == si::model::GameStatus::Unknown) {
		world->setStatus(si::model::GameStatus::Lost);
	}

	world->clearEnemies();

	world->setEndGame();
}

void Controller::moveEnemies() {
	// Check if one of the enemies is at the edge of the field
	bool enemyOnEdge = onEdge();
	if (enemyOnEdge == true) changeDirection();

	// Get the move coordinates
	int move_x, move_y;
	if (world->getEnemies()->direction == si::model::Direction::Right) {
		move_x = world->getProperties().move_amount;
	} 
	else {
		move_x = -1 * world->getProperties().move_amount;
	}

	if (enemyOnEdge) {
		move_y = world->getProperties().move_amount;
	}
	else { 
		move_y = 0;
	}

	// Move the enemies
	for (unsigned int i = 0; i < world->getEnemies()->myEnemies.size(); i++) {
		world->getEnemies()->myEnemies[i]->move(move_x, move_y);
	}
}

bool Controller::onEdge() {
	/* 
	 * This function checks if an enemy is at the edge of the field
	 * This means that an enemy can no longer move to the right or left without falling (partly) of the screen.
	 */

	bool result = false;
	for (unsigned int i = 0; i < world->getEnemies()->myEnemies.size(); i++) {
		if (world->getEnemies()->direction == si::model::Direction::Right) {
			// Check if the enemy is on the edge on the right side of the screen
			if (world->getEnemies()->myEnemies[i]->getX() >= world->getX() - texManager.getTextureSize("enemy").x * world->getEnemies()->myEnemies[i]->getScale()) {
				result = true;
				break;
			}
		}
		else {
			// Check if the enemy is on the edge on the left side of the screen
			if (world->getEnemies()->myEnemies[i]->getX() <= 0) {
				result = true;
				break;
			}
		}
	}
	return result;
}

void Controller::changeDirection() {
	if (world->getEnemies()->direction == si::model::Direction::Right) {
		world->getEnemies()->direction = si::model::Direction::Left;
	}
	else {
		world->getEnemies()->direction = si::model::Direction::Right;
	}
}

void Controller::checkRandomEnemies() {
	/*
	 * This function checks if we need to randomly add an enemy to the world.
	 */

	si::RandomGenerator::instance()->setChance(world->getEnemies()->randomAppearRate);

	int number = si::RandomGenerator::instance()->getNumber();
	if (number == 1) {
		// Create a new enemy
		double scale = world->getEnemies()->myEnemies[0]->getScale();
		int x = rand() % (int)(world->getX() - texManager.getTextureSize("enemy").x * scale);
		int y = 0;/*rand() % (int)(world->getY() - texManager.getTextureSize("enemy").y * scale);*/
		world->setEnemy(x, y, scale);
	}
}
void Controller::checkFireBullets() {
	/*
	 * This function checks if an enemy should shoot or not.
	 */

	si::RandomGenerator::instance()->setChance(world->getEnemies()->bulletRate);
		
	for (unsigned int i = 0; i < world->getEnemies()->myEnemies.size(); i++) {
		int number = si::RandomGenerator::instance()->getNumber();
		if (number == 1) {
			// Get the coordinates for the bullet.
			int x = world->getEnemies()->myEnemies[i]->getX() 
				+ (texManager.getTextureSize("enemy").x * world->getEnemies()->myEnemies[i]->getScale()) / 2 
				- (texManager.getTextureSize("bullet").x * world->getEnemies()->myEnemies[i]->getScale()) / 2;

			int y = world->getEnemies()->myEnemies[i]->getY() 
				+ (texManager.getTextureSize("enemy").y * world->getEnemies()->myEnemies[i]->getScale());
				
			world->setBullet(x, y, si::model::Direction::Down);
		}
	}
}

void Controller::moveBullets() {
	// Move the bullets
	for (unsigned int i = 0; i < world->getBullets().size(); i++) {
		int y = world->getBullets()[i]->getY();
		if (world->getBullets()[i]->getDirection() == si::model::Direction::Up) {
			if (y - world->getProperties().move_amount <= 0) {
				removeBullet(i);
				i--;
			}
			else world->getBullets()[i]->move(0, -1 * world->getProperties().move_amount);
		}
		else if (world->getBullets()[i]->getDirection() == si::model::Direction::Down) {
			if (y + world->getProperties().move_amount > world->getY()) {
				removeBullet(i);
				i--;
			}
			else world->getBullets()[i]->move(0, world->getProperties().move_amount);
		}
		else
			throw std::runtime_error("Bullets can't move to the right or left.");
	}
}

void Controller::checkCollision() {
	// Check if one of the aliens hits the bottom of the screen
	for (unsigned int i = 0; i < world->getEnemies()->myEnemies.size(); i++) {
		if (world->getEnemies()->myEnemies[i]->getY() + texManager.getTextureSize("enemy").y * world->getEnemies()->myEnemies[i]->getScale() >= world->getY()) {
			world->setPlayer(nullptr);
			return;
		}
	}

	// Loop over the bullets
	for (int i = 0; i < world->getBullets().size(); i++) {
		// Get the x- and y-coordinates of the left upper corner (luc) and the right bottom corner (rbc)
		int x_luc_bullet = world->getBullets()[i]->getX();
		int y_luc_bullet = world->getBullets()[i]->getY();
		int x_rbc_bullet = x_luc_bullet + texManager.getTextureSize("bullet").x * world->getBullets()[i]->getScale();
		int y_rbc_bullet = y_luc_bullet + texManager.getTextureSize("bullet").y * world->getBullets()[i]->getScale();

		// Check for collision with an enemy
		if (world->getBullets()[i]->getDirection() == si::model::Direction::Up) {
			for (unsigned int j = 0; j < world->getEnemies()->myEnemies.size(); j++) {
				// Get the x- and y-coordinates of the left upper corner (luc) and the right bottom corner (rbc)
				int x_luc_enemy = world->getEnemies()->myEnemies[j]->getX();
				int y_luc_enemy = world->getEnemies()->myEnemies[j]->getY();
				int x_rbc_enemy = x_luc_enemy + texManager.getTextureSize("enemy").x * world->getEnemies()->myEnemies[j]->getScale();
				int y_rbc_enemy = y_luc_enemy + texManager.getTextureSize("enemy").y * world->getEnemies()->myEnemies[j]->getScale();
				
				// Check for collision
				if (x_luc_bullet >= x_luc_enemy && x_luc_bullet <= x_rbc_enemy
					&& y_luc_bullet >= y_luc_enemy && y_luc_bullet <= y_rbc_enemy) {
					// Remove the bullet and the enemy
					removeBullet(i);
					removeEnemy(j);
					if (world->getEnemies()->myEnemies.size() == 0) {
						world->setPlayer(nullptr);
						return;
					}
					i--;
					break;
				}
			}
		}
		else if (world->getBullets()[i]->getDirection() == si::model::Direction::Down) {
			// Check for collision with the player
			// Get the x- and y-coordinates of the left upper corner (luc) and the right bottom corner (rbc)
			int x_luc_player = world->getPlayer()->getX();
			int y_luc_player = world->getPlayer()->getY();
			int x_rbc_player = x_luc_player + texManager.getTextureSize("player").x * world->getPlayer()->getScale();
			int y_rbc_player = y_luc_player + texManager.getTextureSize("player").y * world->getPlayer()->getScale();
			// Check for collision
			if (x_rbc_bullet >= x_luc_player && x_rbc_bullet <= x_rbc_player
				&& y_rbc_bullet >= y_luc_player && y_rbc_bullet <= y_rbc_player) {
				// Remove a live
				int index = 0;
				for (unsigned int l = 0; l < world->getLives().size(); l++) {
					if (world->getLives()[l]->getX() < world->getLives()[index]->getX()) index = l;
				}
				removeLive(index);
				removeBullet(i);
				if (world->getLives().size() == 0) {
					// End the game
					world->setPlayer(nullptr);
					return;
				}
				i--;
				continue;
			}

			// Check for collision with a shield
			for (std::pair<const std::shared_ptr<si::model::Entity>, std::string >& s : world->getShields()) {
				// s.first is the key, s.second is the value
				int x_luc_shield = s.first->getX();
				int y_luc_shield = s.first->getY();
				int x_rbc_shield = x_luc_shield + texManager.getTextureSize(s.second).x * s.first->getScale();
				int y_rbc_shield = y_luc_shield + texManager.getTextureSize(s.second).y * s.first->getScale();

				if (x_rbc_bullet >= x_luc_shield && x_rbc_bullet <= x_rbc_shield
					&& y_rbc_bullet >= y_luc_shield && y_rbc_bullet <= y_rbc_shield) {
					// Change the observer
					for (std::shared_ptr< si::Observer > o : s.first->getObservers()) {
						if (s.second != "shield3") changeShield(s, o);
						else removeShield(s);
					}
					removeBullet(i);
					i--;
					continue;
				}
			}
		}
		else
			throw std::runtime_error("A bullet cannot move to the " + world->getBullets()[i]->getDirection());
	}
}

void Controller::changeShield(std::pair<const std::shared_ptr<si::model::Entity>, std::string >& s, std::shared_ptr< si::Observer > o) {
	/*
	 * This function changes the shield from shield1 to shield2 and from shield2 to shield3.
	 * It also updates the x- and y-coordinates, so the shield will stay in the same place.
	 */

	std::string from = s.second;
	std::string to;
	if (from == "shield1") to = "shield2";
	else if (from == "shield2") to = "shield3";

	int y = s.first->getY() + (texManager.getTextureSize(from).y * s.first->getScale()) - (texManager.getTextureSize(to).y * s.first->getScale());
	o->update(s.first->getX(), y);
	s.first->setY(y);
	o->update(to);
	world->getShields()[s.first] = to;
}

void Controller::removeLive(int index) {
	// First remove the shape in the buffer from view
	for (unsigned int i = 0; i < world->getLives()[index]->getObservers().size(); i++) {
		world->getLives()[index]->getObservers()[i]->removeFromBuffer();
	}

	// Then remove the live from vector lives
	world->getLives().erase(world->getLives().begin() + index);
}

void Controller::removeShield(std::pair<const std::shared_ptr<si::model::Entity>, std::string >& s) {
	// First remove the shape in the buffer from view
	for (unsigned int i = 0; i < s.first->getObservers().size(); i++) {
		s.first->getObservers()[i]->removeFromBuffer();
	}

	// Then remove the shield from the map
	world->getShields().erase(s.first);
}

void Controller::removeBullet(int index) {
	// First remove the shape in the buffer from View
	for (unsigned int i = 0; i < world->getBullets()[index]->getObservers().size(); i++) {
		world->getBullets()[index]->getObservers()[i]->removeFromBuffer();
	}

	// Then remove the bullet from vector from World
	world->getBullets().erase(world->getBullets().begin() + index);
}

void Controller::removeEnemy(int index) {
	// First remove the shape in the buffer from View
	for (unsigned int i = 0; i < world->getEnemies()->myEnemies[index]->getObservers().size(); i++) {
		world->getEnemies()->myEnemies[index]->getObservers()[i]->removeFromBuffer();
	}

	world->getEnemies()->myEnemies.erase(world->getEnemies()->myEnemies.begin() + index);
}