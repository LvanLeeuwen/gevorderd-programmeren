#ifndef CONTROLLER_H_
#define CONTROLLER_H_

#include <memory>
#include "../si_model/GameObjects.h"
#include "../si_view/View.h"
#include "../si/Stopwatch.h"

namespace si {

	namespace controller {

		class Controller {
		public:
			Controller(std::shared_ptr< si::model::World > _world);
			
			void checkEvents(si::view::View& window, sf::Event& event);

			void checkTicks(si::Stopwatch& stopwatch_main, si::Stopwatch& stopwatch_bullets);

			void endGame(si::view::View& window);

		private:
			std::shared_ptr< si::model::World > world;
			TextureManager texManager;

			void moveEnemies();
			bool onEdge();
			void changeDirection();
			void checkRandomEnemies();
			void checkFireBullets();
			void moveBullets();
			void checkCollision();
			void changeShield(std::pair<const std::shared_ptr<si::model::Entity>, std::string >& s, std::shared_ptr< si::Observer > o);

			void removeLive(int index);
			void removeShield(std::pair<const std::shared_ptr<si::model::Entity>, std::string >& s);
			void removeBullet(int index);
			void removeEnemy(int index);
		};
		
	} /* End of namespace controller */
} /* End of namespace si */

#endif /* CONTROLLER_H_ */