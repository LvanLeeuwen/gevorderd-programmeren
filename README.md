# README #

### What is this repository for? ###

This is the final product for the course `Advanced programming' from the Bachelor Computer Science at University Antwerp. 

### How do I get set up? ###

The bash script `run.sh' contains all necessary commands to run the program. 
Just run this script and everything will get compiled and executed.